// Copyright (c) 2021 Gabriel Hoban
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

const socket = io();
const chat = document.querySelector(".chat-form");
const chatWindow = document.querySelector(".chat-window");
const Input = document.querySelector(".chat-input");

var page_username = "";

let notyf = new Notyf();

const showMessages = (UserObject) => {
  const username = UserObject.username;
  const body = UserObject.body;
  const div = document.createElement("div");
  div.classList.add("render-message");
  div.innerText = `[${username}]: ${body}`;
  chatWindow.appendChild(div);
};

const update_lobby = (list) => {
  lobby = [];
  for (var socket in list) {
    lobby.push(list[socket]);
  }
  console.log(lobby)
  var lobby_list = lobby.join(", ");
  var lobby = document.getElementById("lobby");
  lobby.innerHTML = `Users in lobby: ${lobby_list}`;
};

const login = () => {
  const container = document.getElementById("pubchat");
  const username = document.getElementById('username').value;
  const password = document.getElementById('password').value;

  const object = {
    username: `${username}`,
    password: `${password}`
  };

  socket.emit('login', object);

  socket.on('loginStatus', (loginStatus) => {
    console.log(loginStatus)
    if (loginStatus == false) {
      notyf.error({
        duration: 5000,
        dismissible: true,
        message: 'Login Failed.'
      });
    } else if (loginStatus == true) {
      console.log('Success')
      socket.username = username;
      socket.authenticated = true;

      socket.emit('restore-hist');

      // Show the public message room
      // located in rooms.js

      container.classList.add('d-flex');
      container.classList.remove('d-none');

      // Toast
      console.log('Sucessful Login Attempt');

      notyf.success({
        duration: 5000,
        dismissible: true,
        message: 'Login Successful.'
      });

      // Hide login modal
      $('#loginForm').modal('hide');

      return true;
    }
  });
};

socket.on('restore-hist', (arr) => {
  console.log('Restoring History ' + JSON.stringify(arr))
  if (arr && arr.length > 0){
    for (let index in arr) {
      let mess = arr[index].body;
      let user = arr[index].username;
      console.log(mess + ' // ' + user)
      const div = document.createElement("div");
      div.classList.add("render-message");
      div.innerText = `[${user}]: ${mess}`;
      chatWindow.appendChild(div);
    }
  }
});

const signup = () => {
  const container = document.getElementById("pubchat");
  const username = document.getElementById('rusername').value;
  const password1 = document.getElementById('password1').value;
  const password2 = document.getElementById('password2').value;

  if (username.length < 5) {
    console.log('Username must be 5 or more charactors');
    notyf.error({
      duration: 5000,
      dismissible: true,
      message: 'Username must be a minimum of 5 charactors.'
    });
    return false;
  }

  if (password1 !== password2) {
    console.log('Password do not match.');
    notyf.error({
      duration: 5000,
      dismissible: true,
      message: 'Passwords do not match.'
    });
    return false;
  }

  if (password1.length < 5) {
    console.log('Password must be 5 or more charactors');
    notyf.error({
      duration: 5000,
      dismissible: true,
      message: 'Password must be a minimum of 5 charactors.'
    });
    return false;
  }

  const registerObject = JSON.stringify({
    username: `${username}`,
    password: `${password1}`
  });

  // Sumbit registration to the backend, logic for adding to the database implemented here
  socket.emit('signup', registerObject);

  // On backend response to registration.
  socket.on('signupStatus', (registerStatus) => {
    if (registerStatus === false) {
      notyf.error({
        duration: 5000,
        dismissible: true,
        message: 'Registration Failed.'
      });

      return false;
    } else if (registerStatus === true) {
      socket.username = username;
      socket.authenticated = true;

      container.classList.add('d-block');
      container.classList.remove('d-none');

      notyf.success({
        duration: 5000,
        dismissible: true,
        message: 'Registration Successful.'
      });

      // Hide login modal
      $('#registerForm').modal('hide');

      return true;
    }
  });
};


window.addEventListener("load", function () {
  $("#loginForm").modal("show");
});

socket.on("chat", (message) => {
  // make sure to modify this
  showMessages(message);
});

socket.on("notifyInvalidLogin", (username) => {
  $("#error").modal("show");
  console.log("Invalid login by user: " + username);
});

socket.on("username_list", (list) => {
  update_lobby(list);
});

chat.addEventListener("submit", (event) => {
  event.preventDefault();
  const UserObject = {
    username: socket.username,
    body: Input.value
  }
  socket.emit("chat", UserObject);
  Input.value = "";
});

const loginSubmitButton = document.getElementById('loginButton');
loginSubmitButton.addEventListener('click', (event) => {
  login();

  // prevent default window refresh upon submission
  event.preventDefault();
});

const signUpSubmitButton = document.getElementById('registerButton');
signUpSubmitButton.addEventListener('click', (event) => {
  signup();

  // prevent default window refresh upon submission
  event.preventDefault();
});

const Switch2Register = document.getElementById('swch2register');
Switch2Register.addEventListener('click', (event) => {
  // Hide login modal + show register modal
  $('#loginForm').modal('hide');
  $('#registerForm').modal('show');
  event.preventDefault();
});

const Switch2Login = document.getElementById('swch2login');
Switch2Login.addEventListener('click', (event) => {
  // Hide register modal + show login modal
  $('#loginForm').modal('show');
  $('#registerForm').modal('hide');
  event.preventDefault();
});
