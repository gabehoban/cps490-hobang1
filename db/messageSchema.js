const mongoose = require('mongoose');

const MessageSchema = new mongoose.Schema({
    username: String,
    body: String,
}, { collection: 'PublicMessages' } )

module.exports = mongoose.model('Message', MessageSchema);
