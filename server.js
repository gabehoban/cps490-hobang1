// Copyright (c) 2021 gabehoban
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

var express = require("express");
var app = require("express")();
var server = require("http").Server(app);
var io = require("socket.io")(server);
var cors = require("cors");
var mongoose = require('mongoose');

const User = require('./db/userSchema');
const Message = require('./db/messageSchema');

const { getDefaultSettings } = require("http2");

require("dotenv").config();

//#####################################
//## Socket.io Functions
//#####################################
const users = {};

// On new websocket connection
io.on("connection", (socket) => {
  socket.on("chat", (message) => {
    storeMessage({username: message.username, body: message.body});
    io.emit("chat", message);
  });

  socket.on('restore-hist', async () => {
    const arr = await loadMessage();
    console.log('Restoring ' + arr.length + ' messages.')
    socket.emit('restore-hist', arr)
  });

  socket.on("login", (object) => {
    console.log(object)
    console.log("Logging in " + object.username);
    if (checkLogin(object.username, object.password)) {
      io.emit('loginStatus', true)
      users[socket.id] = object.username;
      io.emit("username_list", users);
    } else {
      io.emit('loginStatus', false);
    }

  });

  socket.on("signup", (object) => {
    console.log("Signing Up " + object.username);
    if (signup(object.username, object.password)) {
      io.emit('signupStatus', true)
      users[socket.id] = object.username;
      io.emit("username_list", users);
    } else {
      io.emit('signupStatus', false);
    }

  });

  socket.on("invalidLogin", (username) => {
    console.log("Invalid Login for user " + username);
    io.emit("notifyInvalidLogin", username);
  });
  socket.on("disconnect", () => {
    delete users[socket.id];
    io.emit("username_list", users);
  });
});

//#####################################
//## Database Initialization
//#####################################

const DB_USERNAME = process.env.DB_USERNAME;
const DB_PASSWORD = process.env.DB_PASSWORD;
const DB_CLUSTER  = process.env.DB_CLUSTER;
const DB_NAME     = process.env.DB_NAME;

const mongodURI = `mongodb+srv://${DB_USERNAME}:${DB_PASSWORD}@${DB_CLUSTER}/${DB_NAME}`;
mongoose.connect(mongodURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    retryWrites: true,
}).then(client => {
    console.log(">>> Database conneted.");
}).catch(err => {
    console.warn(mongodURI);
    console.warn(`Error occurred while connecting to mongodb: ${err}`);
    process.exit(1);
});


const checkLogin = async (username, password) => {
  const foundUser = await User.findOne ({ username: username });
  if (foundUser) {
    if (foundUser.password == password) {
      return true
    } else {
      return false
    }
  } else {
    return false
  }
}

const storeMessage = async (MessageObject) => {
  console.log('Storing \n' + MessageObject)
  const newMessage = new Message({
    username: MessageObject.username,
    body: MessageObject.body
  });

  newMessage.save();

  const anyUser = await Message.find({});
  console.log(JSON.stringify(anyUser))
}

const loadMessage = async () => {
  let arr = [];
  for await (const doc of Message.find()) {
    arr.push(doc)
  }
  return arr;
}

const signup = async (username, password) => {
  const anyUser = await User.findOne({ username: username });
  console.log(anyUser);
  if (anyUser !== null) {
    return false;
  } else {
    // Create new user object with credentials.
    const newUser = new User({
      username: username,
      password: password,
    });

    console.log('New user registration: \n' + newUser);

    // Save the user to the database.
    newUser.save();

    // Some error checking should probally be done here.
    return true;
  }
}

//#####################################
//## Express Server Routes
//#####################################
const PORT = process.env.PORT || 3000;

app.use(cors());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Credentials", true);
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin,X-Requested-With,Content-Type,Accept,content-type,application/json"
  );
  next();
});
app.use(express.json());
app.use(express.static('public'));

server.listen(PORT, () =>
  console.log("Backend server is running on http://localhost:" + PORT)
);
