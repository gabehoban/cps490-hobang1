FROM node:latest

WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install

COPY . .
RUN echo "Creating a Docker image by hobang1@udayton.edu"

CMD ["npm", "start"]