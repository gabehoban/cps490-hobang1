# README

### Gabe Hoban

![Profile Pic](https://avatars.githubusercontent.com/u/12862213?v=4){width=100 height=100}

hobang1@udayton.edu

CPS 490 - Capstone 1

## Repository Information

This is the private repository of Gabe Hoban. It will be
used as a landing page for my bitbucket user.

My Bitbucket Repositories:
- [Private Repsitory](https://bitbucket.org/gabehoban/cps490-hobang1/src/master/)
- [Team Repository](https://bitbucket.org/gabehoban/cps490f21-team5/src/master/)

Capstone Trello Board:
- [Team Board URL](https://trello.com/b/OnhYeymu/scrum)

## Use Cases

UML Use Case Example

```uml
@startuml
left to right direction
package Users {
  actor unauthorized as unauth
  actor authorized as auth
}
package "Use Cases" {
  usecase "User must first register" as UC1
  usecase "Send Message" as UC2
  usecase "Recieve Message" as UC3
}
unauth --> UC1
auth --> UC2
@enduml
```

![PNG](./assets/UML.png)
